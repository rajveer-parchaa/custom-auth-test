// bearer.strategy.ts

import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-http-bearer';
import { verify } from 'jsonwebtoken';

const secret = 'your-secret-key';

@Injectable()
export class BearerStrategy extends PassportStrategy(Strategy, 'bearer') {
  constructor() {
    super();
  }

  async validate(token: string) {
    try {
      const decoded = verify(token, secret);
      // You can also perform additional validation if needed, like checking if the clientId exists in your storage
      return decoded;
    } catch (error) {
      throw new UnauthorizedException('Invalid access token');
    }
  }
}
