import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-oauth2-client-password';

const clients = [
    { clientId: 'abc', clientSecret: 'abcd', name: 'Your Client' },
];
  

async function validateClient(clientId: string, clientSecret: string): Promise<any> {
    const client = clients.find(
      (c) => c.clientId === clientId && c.clientSecret === clientSecret,
    );
    return client;
}

@Injectable()
export class ClientPasswordStrategy extends PassportStrategy(Strategy, 'client-password') {
    constructor() {
        super({
          passReqToCallback: true,
          clientIdField: 'client_id',
          clientSecretField: 'client_secret',
        });
    }

    async validate(req, clientId: string, clientSecret: string, done: (error: any, client?: any, info?: any) => void) {
        console.log('Validating client:', clientId, clientSecret);
        const client = await validateClient(clientId, clientSecret);
        if (!client) {
          console.log('Client not found or invalid credentials');
          return done(null, false);
        }
        console.log('Client found:', client);
        req.client = client; // Add this line
        return done(null, client);
      }       
}
