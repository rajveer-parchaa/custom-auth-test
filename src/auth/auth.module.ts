// auth.module.ts

import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { AccessTokenModule } from 'src/access-token/access-token.module';
import { BearerStrategy } from './bearer.strategy';
import { ClientPasswordStrategy } from './client-password.strategy';

@Module({
  imports: [PassportModule, AccessTokenModule],
  providers: [BearerStrategy, ClientPasswordStrategy],
  exports: [BearerStrategy, ClientPasswordStrategy],
})
export class AuthModule {}
