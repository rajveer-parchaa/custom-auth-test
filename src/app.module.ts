import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { AccessTokenModule } from './access-token/access-token.module';

@Module({
  imports: [AuthModule, AccessTokenModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
