import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  getHelloOne(): string {
    return 'Hello World! from one';
  }


  getHelloTwo(): string {
    return 'Hello World! from two';
  }


  getHelloThree(): string {
    return 'Hello World! from three';
  }


  getHelloFour(): string {
    return 'Hello World! from four';
  }
}
