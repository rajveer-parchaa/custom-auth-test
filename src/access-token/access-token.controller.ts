// access-token.controller.ts

import { Controller, Request, Post, UseGuards, Body, BadRequestException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AccessTokenService } from './access-token.service';

@Controller('access-token')
export class AccessTokenController {
  constructor(private readonly accessTokenService: AccessTokenService) {}

    @Post()
    async create(@Request() req, @Body('grant_type') grantType: string) {
    console.log('Received request:', req.client, grantType);
    if (grantType !== 'client_credentials') {
        throw new BadRequestException('Invalid grant_type');
    }
    return this.accessTokenService.createAccessToken(req.client);
    }
}
