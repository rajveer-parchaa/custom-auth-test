import { Injectable } from '@nestjs/common';
import { sign } from 'jsonwebtoken';

function generateAccessToken(client): string {
  const payload = {
    clientId: client.clientId,
    name: client.name,
  };
  const secret = 'your-secret-key';
  const expiresIn = 3600; // 1 hour

  return sign(payload, secret, { expiresIn });
}


@Injectable()
export class AccessTokenService {
  async createAccessToken(client) {
    // Generate an access token based on client information and store it
    // You can use JWT or any other token format
    const accessToken = generateAccessToken(client);
    return { access_token: accessToken, token_type: 'Bearer', expires_in: 3600 };
  }
}
