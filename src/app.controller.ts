import { Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthGuard } from '@nestjs/passport';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @UseGuards(AuthGuard('bearer'))
  @Get('one')
  getHelloOne(): string {
    return this.appService.getHelloOne();
  }

  @Get('two')
  getHelloTwo(): string {
    return this.appService.getHelloTwo();
  }

  @Get('three')
  getHelloThree(): string {
    return this.appService.getHelloThree();
  }

  @Get('four')
  getHelloFour(): string {
    return this.appService.getHelloFour();
  }
  
}
