<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Custom Authentication Module
This is a custom authentication module built using NestJS, Passport, and JSON Web Tokens (JWT). This module implements OAuth 2.0 Client Credentials Flow for authentication and token generation. The module consists of various strategies, controllers, and services to handle the authentication process.

## Prerequisites
- Node.js 12.x or later
- npm or yarn
- NestJS CLI
## Features
- OAuth 2.0 Client Credentials Flow
- Token generation using JSON Web Tokens (JWT)
- Token validation and expiration handling
- Passport strategies for handling client authentication
- Example protected routes

## Installation

Clone this repository:

```bash
$ git clone https://gitlab.com/rajveer-parchaa/custom-auth-test
```

Change into the project directory:
```bash
$ cd custom-auth-test
```
Install dependencies:

```bash
$ npm install
```

Set environment variables in a .env file in the project root (use .env.example as a template).


## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Usage
- Obtain an access token by sending a request to the /access-token endpoint with your client ID and client secret in the Authorization header:

```bash
$ curl -X POST -H "Authorization: Basic BASE64_ENCODED_CLIENT_ID_AND_SECRET" http://localhost:3000/access-token
```
Replace BASE64_ENCODED_CLIENT_ID_AND_SECRET with the Base64 encoded string of client_id:client_secret.

- Use the access token to access protected endpoints:

```bash
$ curl -X GET -H "Authorization: Bearer YOUR_ACCESS_TOKEN" http://localhost:3000/one
```
Replace YOUR_ACCESS_TOKEN with the actual access token obtained from the previous step.

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [RAJVEER](rajveer@parchaa.com)
- Website - [https://parchaa.com](https://parchaa.com/)

## License

Nest is [MIT licensed](LICENSE).
